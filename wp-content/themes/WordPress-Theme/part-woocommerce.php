<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns text-center title_products">
				<img src="<?php echo get_site_url(); ?>/wp-content/themes/WordPress-Theme/build/title_products.png">
			</div>
			<!--<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'left' ); ?>
			</div>-->
			<div class="small-12 columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->