<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns text-center show-for-small-only">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
		</div>
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-2 columns"></div>
			<div class="small-12 medium-8 columns">
				<?php dynamic_sidebar( 'menu' ); ?>
			</div>
			<div class="small-12 medium-2 columns text-right">
				<?php dynamic_sidebar( 'quote' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->